/**
 * @author tla
 * @see {@link https://framagit.org/tla}
 *
 * awesome-bubble
 * @see {@link https://framagit.org/tla/awesome-bubble}
 *
 * @license MIT
 * @see {@link https://framagit.org/tla/awesome-bubble/blob/master/LICENSE}
 *
 * @use js-tools-kit
 * @see {@link https://framagit.org/tla/js-tools-kit}
 *
 * @use css-rule
 * @see {@link https://framagit.org/tla/css-rule}
 *
 * Material design bubble effect
 *
 * @method _awesomeBubble
 * @argument {HTMLElement} element
 * @argument {Object} [setting]
 * @return {constructor}
 *
 * @example
 *  var element = document.getElementById('toto');
 *  var bubble = _awesomeBubble( element ); // finish !
 *
 *  // setting
 *
 *  bubble = _awesomeBubble( element , {
 *      color: 'rgba(0,0,0,0.1)' // custom the bubble color, default "rgba(100, 255, 255, 0.3)"
 *      colorEnd: 'rgba(0,0,0,0.1)' // custom the bubble color transition, default null
 *      duration:   500 // custom the duration bubble effect in ms, default 700
 *      number: 3 // number of bubble per click, default 1
 *      interval: 150 // interval between bubbles ( if number > 1 ) in ms, default 100
 *      stayOnFocus: 150 // persistent bubble, default false
 *      document: document / iframe.contentDocument // document object
 *  } );
 *
 *  // can do a fucking rainbow ( and destroy your eyes )
 *  bubble = _awesomeBubble( element , {
 *      duration:   2000 ,
 *      color: [
 *          'rgb(50,0,0)' ,
 *          'rgb(150,0,0)' ,
 *          'rgb(250,0,0)' ,
 *          'rgb(0,50,0)' ,
 *          'rgb(0,150,0)' ,
 *          'rgb(0,250,0)' ,
 *          'rgb(0,0,50)' ,
 *          'rgb(0,0,150)' ,
 *          'rgb(0,0,250)'
 *      ]
 *  } );
 *
 *  // disable
 *  bubble.disable();
 *
 *  // enable
 *  bubble.enable();
 *
 *  // kill
 *  bubble.destroy();
 *
 *  // change settings dynamicaly (you can't change "document" setting)
 *  bubble.setParams( "color" , ... );
 * */

// TODO faire un readme

( function () {
    
    'use strict';
    
    var global ,
        hasProperty = Object.prototype.hasOwnProperty;
    
    try {
        global = Function( 'return this' )() || ( 42, eval )( 'this' );
    } catch ( e ) {
        global = window;
    }
    
    /**
     * js-tools-kit
     * @license MIT
     * @see {@link https://framagit.org/tla/js-tools-kit}
     * */
    ( function () {
        function _defineProperty ( obj , key , prop ) {
            Object.defineProperty( obj , key , {
                configurable : false ,
                enumerable : true ,
                writable : false ,
                value : prop
            } );
        }
        
        /**
         * @readonly
         * @property {Window} global
         * */
        _defineProperty( global , 'global' , global );
        
        /**
         * @readonly
         * @function _defineProperty
         * @param {Object} obj
         * @param {string} key
         * @param {*} prop
         * */
        _defineProperty( global , '_defineProperty' , _defineProperty );
        
        /**
         * @readonly
         * @function isset
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isset' , function ( _ ) {
            return _ !== undefined;
        } );
        
        /**
         * @readonly
         * @function isnull
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isnull' , function ( _ ) {
            return _ === null;
        } );
        
        /**
         * @readonly
         * @function exist
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'exist' , function ( _ ) {
            return isset( _ ) && !isnull( _ );
        } );
        
        /**
         * @readonly
         * @function isdate
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isdate' , function ( _ ) {
            return exist( _ ) && _ instanceof Date;
        } );
        
        /**
         * @readonly
         * @function isarray
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isarray' , function ( _ ) {
            return exist( _ ) && Array.isArray( _ );
        } );
        
        /**
         * @readonly
         * @function isobject
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isobject' , function ( _ ) {
            return exist( _ ) &&
                _ instanceof Object && _.toString() === '[object Object]' &&
                _.constructor.hasOwnProperty( 'defineProperty' );
        } );
        
        /**
         * @readonly
         * @function isstring
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isstring' , function ( _ ) {
            return exist( _ ) && ( typeof _ === 'string' || _ instanceof global.String );
        } );
        
        /**
         * @readonly
         * @function isfillstring
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isfillstring' , function ( _ ) {
            return isstring( _ ) && !!_.trim();
        } );
        
        /**
         * @readonly
         * @function isboolean
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isboolean' , function ( _ ) {
            return exist( _ ) && ( typeof _ === 'boolean' || _ instanceof global.Boolean );
        } );
        
        /**
         * @readonly
         * @function isinteger
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isinteger' , function ( _ ) {
            return exist( _ ) && Number.isInteger( _ );
        } );
        
        /**
         * @readonly
         * @function isfloat
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isfloat' , function ( _ ) {
            return exist( _ ) && typeof _ == 'number' && isFinite( _ );
        } );
        
        /**
         * @readonly
         * @function isfunction
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isfunction' , function ( _ ) {
            return exist( _ ) && ( typeof _ === 'function' || _ instanceof global.Function );
        } );
        
        /**
         * @readonly
         * @function isregexp
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isregexp' , function ( _ ) {
            return exist( _ ) && ( _ instanceof global.RegExp );
        } );
        
        /**
         * @readonly
         * @function isnodelist
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isnodelist' , function ( _ ) {
            return exist( _ ) && (
                ( _ instanceof global.NodeList || _ instanceof global.HTMLCollection ) ||
                ( !isset( _.slice ) && isset( _.length ) && typeof _ === 'object' )
            );
        } );
        
        /**
         * @readonly
         * @function isdocument
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isdocument' , function ( _ ) {
            return exist( _ ) && exist( _.defaultView ) && _ instanceof _.defaultView.HTMLDocument;
        } );
        
        /**
         * @readonly
         * @function iswindow
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'iswindow' , function ( _ ) {
            return exist( _ ) && ( _ === global || ( exist( _.window ) &&
                exist( _.window.constructor ) && _ instanceof _.window.constructor ) );
        } );
        
        /**
         * @readonly
         * @function getdocument
         * @param {*} _
         * @return {?HTMLDocument}
         * */
        _defineProperty( global , 'getdocument' , function ( _ ) {
            var tmp;
            
            if ( exist( _ ) ) {
                
                if ( isdocument( _ ) ) {
                    return _;
                }
                
                if ( isdocument( tmp = _.ownerDocument ) ) {
                    return tmp;
                }
                
                if ( iswindow( _ ) ) {
                    return _.document;
                }
                
            }
            
            return null;
        } );
        
        /**
         * @readonly
         * @function getwindow
         * @param {*} _
         * @return {!Window}
         * */
        _defineProperty( global , 'getwindow' , function ( _ ) {
            var tmp;
            
            if ( iswindow( _ ) ) {
                return _;
            }
            
            if ( tmp = getdocument( _ ) ) {
                return tmp.defaultView;
            }
            
            return null;
        } );
        
        /**
         * @readonly
         * @function isnode
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isnode' , function ( _ ) {
            var doc , tag;
            
            if ( ( doc = getdocument( _ ) ) && isstring( tag = _.tagName ) ) {
                return ( doc.createElement( tag ) instanceof _.constructor ||
                    getdocument( global ).createElement( tag ) instanceof _.constructor );
            }
            
            return false;
        } );
        
        /**
         * @readonly
         * @property {boolean} eventPassiveSupport
         * */
        ( function () {
            var tmp ,
                sup = false ,
                document = getdocument( global );
            
            if ( document ) {
                tmp = document.createDocumentFragment();
                
                var empty = function () {} , setting = {
                    get passive () {
                        sup = true;
                        return false;
                    }
                };
                
                tmp.addEventListener( 'click' , empty , setting );
            }
            
            _defineProperty( global , 'eventPassiveSupport' , sup );
        } )();
        
        /**
         * @readonly
         * @function domLoad
         * @param {function} callback
         * */
        _defineProperty( global , 'domLoad' , ( function () {
            var ready = false ,
                stackfunction = [] ,
                document = getdocument( global );
            
            function stack ( callback ) {
                if ( isfunction( callback ) ) {
                    if ( ready ) {
                        return callback.call( document );
                    }
                    
                    stackfunction.push( callback );
                }
            }
            
            function load () {
                if ( !ready ) {
                    ready = true;
                    
                    stackfunction.forEach( function ( callback ) {
                        callback.call( document );
                    } );
                    
                    stackfunction = null;
                    
                    if ( document ) {
                        document.removeEventListener( 'DOMContentLoaded' , load , true );
                        document.removeEventListener( 'DomContentLoaded' , load , true );
                        document.removeEventListener( 'load' , load , true );
                    }
                    
                    global.removeEventListener( 'load' , load , true );
                }
            }
            
            if ( document ) {
                document.addEventListener( 'DOMContentLoaded' , load , true );
                document.addEventListener( 'DomContentLoaded' , load , true );
                document.addEventListener( 'load' , load , true );
            }
            
            global.addEventListener( 'load' , load , true );
            
            return stack;
        } )() );
        
        /**
         * @readonly
         * @function matchesSelector
         * @param {HTMLElement|Element|Node} e - element
         * @param {string} s - css selector
         * @return {!boolean}
         * */
        _defineProperty( global , 'matchesSelector' , ( function () {
            var document = getdocument( global ) ,
                match = null ,
                tmp;
            
            if ( document ) {
                tmp = document.createElement( 'div' );
                
                match = tmp[ 'matches' ] && 'matches' ||
                    tmp[ 'matchesSelector' ] && 'matchesSelector' ||
                    tmp[ 'webkitMatchesSelector' ] && 'webkitMatchesSelector' ||
                    tmp[ 'mozMatchesSelector' ] && 'mozMatchesSelector' ||
                    tmp[ 'oMatchesSelector' ] && 'oMatchesSelector' ||
                    tmp[ 'msMatchesSelector' ] && 'msMatchesSelector' || null;
            }
            
            return function ( e , s ) {
                if ( !match || !e[ match ] ) {
                    return false;
                }
                
                return e[ match ]( s );
            };
        } )() );
        
        /**
         * @readonly
         * @function nl2br
         * @param {string} str
         * @return {!string}
         * */
        _defineProperty( global , 'nl2br' , function ( str ) {
            if ( isstring( str ) ) {
                return str
                    .replace( /\n/g , "<br/>" )
                    .replace( /\t/g , "&nbsp;&nbsp;&nbsp;&nbsp;" );
            }
            
            return '';
        } );
        
        /**
         * @readonly
         * @function inarray
         * @param {*} _in
         * @param {Array} array
         * @return {!boolean}
         * */
        _defineProperty( global , 'inarray' , function ( _in , array ) {
            return array.indexOf( _in ) >= 0;
        } );
        
        /**
         * @readonly
         * @function arrayunique
         * @param {Array} _
         * @return {!Array}
         * */
        _defineProperty( global , 'arrayunique' , function ( _ ) {
            for ( var n = [] , i = 0 , l = _.length ; i < l ; i++ ) {
                if ( !inarray( _[ i ] , n ) ) {
                    n.push( _[ i ] );
                }
            }
            return n;
        } );
        
        /**
         * @readonly
         * @function typedtoarray
         * @param {*} arg
         * @return {!Array}
         * */
        _defineProperty( global , 'typedtoarray' , function ( arg ) {
            return Array.apply( null , arg );
        } );
        
        /**
         * @readonly
         * @function getallchildren
         * @param {HTMLElement|Element|Node} element
         * @return {!NodeList}
         * */
        _defineProperty( global , 'getallchildren' , function ( element ) {
            return element.getElementsByTagName( '*' );
        } );
        
        /**
         * @readonly
         * @function emptyNode
         * @param {HTMLElement|Element|Node} element
         * @return {!(HTMLElement|Element|Node)}
         * */
        _defineProperty( global , 'emptyNode' , function ( element ) {
            var c;
            
            while ( c = element.firstChild ) {
                element.removeChild( c );
            }
            
            return element;
        } );
        
        /**
         * @readonly
         * @function closest
         * @param {HTMLElement|Element|Node} element
         * @param {string} selector
         * @return {?(HTMLElement|Element|Node)}
         * */
        _defineProperty( global , 'closest' , ( function () {
            var document = getdocument( global );
            
            if ( document && isfunction( document.createElement( 'div' ).closest ) ) {
                return function ( element , selector ) {
                    if ( !isnode( element ) ) {
                        return null;
                    }
                    
                    return element.closest( selector );
                };
            }
            
            return function ( element , selector ) {
                if ( !isnode( element ) ) {
                    return null;
                }
                
                if ( matchesSelector( element , selector ) ) {
                    return element;
                }
                
                while ( isnode( element = element.parentNode ) ) {
                    if ( matchesSelector( element , selector ) ) {
                        return element;
                    }
                }
                
                return null;
            };
        } )() );
        
        /**
         * @readonly
         * @function randomstring
         * @return {!string}
         * */
        _defineProperty( global , 'randomstring' , function () {
            return '_' + Math.random().toString( 30 ).substring( 2 );
        } );
        
        /**
         * @readonly
         * @function between
         * @param {number} min
         * @param {number} max
         * @return {!number}
         * */
        _defineProperty( global , 'between' , function ( min , max ) {
            return Math.floor( Math.random() * ( max - min + 1 ) + min );
        } );
        
        /**
         * @readonly
         * @function round
         * @param {number} int
         * @param {number} after
         * @return {!number}
         * */
        _defineProperty( global , 'round' , function ( int , after ) {
            return parseFloat( int.toFixed( isinteger( after ) ? after : int.toString().length ) );
        } );
        
        /**
         * @readonly
         * @function wrapint
         * @param {number} int
         * @param {number} howmany
         * @return {!number}
         * */
        _defineProperty( global , 'wrapinteger' , function ( int , howmany ) {
            int = int.toString();
            
            while ( int.length < howmany ) {
                int = '0' + int;
            }
            
            return int;
        } );
        
        /**
         * @readonly
         * @function jsonparse
         * @param {string} str
         * @return {?Object}
         * */
        _defineProperty( global , 'jsonparse' , function ( str ) {
            var result = null ,
                masterKey;
            
            try {
                result = JSON.parse( str );
            } catch ( e ) {
                try {
                    masterKey = randomstring();
                    
                    global[ masterKey ] = null;
                    
                    eval( 'global[ masterKey ] = ' + str );
                    
                    if ( isobject( global[ masterKey ] ) ) {
                        result = global[ masterKey ] || null;
                    }
                } catch ( e ) {
                    result = null;
                } finally {
                    delete global[ masterKey ];
                }
            }
            
            return result;
        } );
        
        if ( !isfunction( global.setImmediate ) ) {
            ( function () {
                var message = 0 ,
                    register = {} ,
                    prefix = randomstring() + '.immediate';
                
                function getLocation ( location ) {
                    try {
                        if ( location.origin && location.origin != 'null' ) {
                            return location.origin;
                        }
                        
                        if ( location.ancestorOrigins && location.ancestorOrigins.length ) {
                            return location.ancestorOrigins[ 0 ];
                        }
                    } catch ( _ ) {
                        return null;
                    }
                }
                
                var origin = ( function () {
                    var init = global;
                    
                    while ( true ) {
                        var tmp = getLocation( init.location );
                        
                        if ( tmp ) {
                            return tmp;
                        }
                        
                        if ( iswindow( init.parent ) ) {
                            init = init.parent;
                            continue;
                        }
                        
                        return '';
                    }
                } )();
                
                try {
                    global.postMessage( prefix + 1 , function () {} );
                } catch ( e ) {
                    return ( function () {
                        _defineProperty( global , 'setImmediate' , global.setTimeout );
                        _defineProperty( global , 'clearImmediate' , global.clearTimeout );
                    } )();
                }
                
                global.addEventListener( 'message' , function ( event ) {
                    if ( origin === event.origin ) {
                        var i = event.data;
                        
                        if ( isfunction( register[ i ] ) ) {
                            register[ i ]();
                            delete register[ i ];
                            event.stopImmediatePropagation();
                        }
                    }
                } , eventPassiveSupport ? { capture : true , passive : true } : true );
                
                /**
                 * @readonly
                 * @function setImmediate
                 * @param {function} fn
                 * @return {!number}
                 * */
                _defineProperty( global , 'setImmediate' , function ( fn ) {
                    var id = ++message;
                    
                    register[ prefix + id ] = fn;
                    global.postMessage( prefix + id , origin );
                    
                    return id;
                } );
                
                /**
                 * @readonly
                 * @function clearImmediate
                 * @param {number} id
                 * @return void
                 * */
                _defineProperty( global , 'clearImmediate' , function ( id ) {
                    if ( register[ prefix + id ] ) {
                        delete register[ prefix + id ];
                    }
                } );
            } )();
        }
        
        /**
         * @readonly
         * @function getstyleproperty
         * @param {HTMLElement|Element|Node} element
         * @param {string} property
         * @return {?string}
         * */
        _defineProperty( global , 'getstyleproperty' , function ( element , property ) {
            var val , win;
            
            if ( isnode( element ) ) {
                
                if ( val = element.style[ property ] ) {
                    return val;
                }
                
                if ( val = element.style.getPropertyValue( property ) ) {
                    return val;
                }
                
                if ( win = getwindow( element ) ) {
                    return win.getComputedStyle( element ).getPropertyValue( property ) || null;
                }
                
            }
            
            return null;
        } );
        
        /**
         * @readonly
         * @function splitTrim
         * @param {string} str
         * @param {RegExp|string} spl
         * @return {Array}
         * */
        _defineProperty( global , 'splitTrim' , function ( str , spl ) {
            var ar = str.split( spl );
            
            for ( var i = 0 , r = [] , tmp ; i < ar.length ; ) {
                if ( tmp = ar[ i++ ].trim() ) {
                    r.push( tmp );
                }
            }
            
            return r;
        } );
        
        /**
         * @readonly
         * @function clonearray
         * @param {Array} _
         * @return {!Array}
         * */
        _defineProperty( global , 'clonearray' , function ( _ ) {
            return _.slice( 0 );
        } );
        
        /**
         * @readonly
         * @function toarray
         * @param {*} _
         * @return {!Array}
         * */
        _defineProperty( global , 'toarray' , function ( _ ) {
            return isset( _ ) ? ( isarray( _ ) ? _ : [ _ ] ) : [];
        } );
        
        /**
         * @readonly
         * @function counterCallback
         * @param {number} counter
         * @param {function} callback
         * @return {!Array}
         * */
        _defineProperty( global , 'counterCallback' , function ( counter , callback ) {
            var current = 0;
            
            return function () {
                if ( ++current == counter ) {
                    callback();
                }
            };
        } );
        
        /**
         * @readonly
         * @function regexpEscapeString
         * @param {string} str
         * @return {!string}
         * */
        _defineProperty( global , 'regexpEscapeString' , function ( str ) {
            if ( !isstring( str ) ) {
                return '';
            }
            
            return str.replace( /\\/gi , '\\\\' ).replace( /([.\[\](){}^?*|+\-$])/gi , '\\$1' );
        } );
        
        /**
         * @readonly
         * @function htmlEscapeString
         * @param {string} str
         * @return {!string}
         * */
        _defineProperty( global , 'htmlEscapeString' , function ( str ) {
            if ( !isstring( str ) ) {
                return '';
            }
            
            return str
                .replace( /&/gi , '&amp;' )
                .replace( /"/gi , '&quot;' )
                .replace( /'/gi , '&apos;' )
                .replace( /</gi , '&lt;' )
                .replace( />/gi , '&gt;' );
        } );
        
        /**
         * @readonly
         * @function resetregexp
         * @param {RegExp} _
         * @return {?RegExp}
         * */
        _defineProperty( global , 'resetregexp' , function ( _ ) {
            if ( !isregexp( _ ) ) {
                return null;
            }
            
            return _.lastIndex = _.index = 0, _;
        } );
        
        /**
         * @readonly
         * @function overrideObject
         * @return {!Object}
         * */
        _defineProperty( global , 'overrideObject' , function () {
            var result = {} ,
                array = typedtoarray( arguments );
            
            if ( array.length <= 0 ) {
                return result;
            }
            
            if ( array.length == 1 ) {
                return isobject( array[ 0 ] ) ? array[ 0 ] : result;
            }
            
            for ( var i = 0 , l = array.length ; i < l ; i++ ) {
                forin( array[ i ] , function ( key , value ) {
                    result[ key ] = value;
                } );
            }
            
            return result;
        } );
        
        /**
         * @readonly
         * @function isEmptyObject
         * @param {Object} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isEmptyObject' , function ( _ ) {
            if ( isobject( _ ) ) {
                for ( var key in _ ) {
                    if ( hasProperty.call( _ , key ) ) {
                        return false;
                    }
                }
                
                return true;
            }
            
            return false;
        } );
        
        /**
         * @readonly
         * @function forin
         * @param {Object} obj
         * @param {function} callback
         * @return void
         * */
        _defineProperty( global , 'forin' , function ( obj , callback ) {
            var key , value;
            
            if ( isobject( obj ) ) {
                for ( key in obj ) {
                    if ( hasProperty.call( obj , key ) && isset( value = obj[ key ] ) ) {
                        callback( key , value , obj );
                    }
                }
            }
        } );
        
        /**
         * @readonly
         * @function foreach
         * @param {Array} arr
         * @param {function} callback
         * @return void
         * */
        _defineProperty( global , 'foreach' , function ( arr , callback ) {
            if ( isarray( arr ) ) {
                for ( var i = 0 , l = arr.length ; i < l ; i++ ) {
                    callback( arr[ i ] , i , arr );
                }
            }
        } );
        
        /**
         * @readonly
         * @function for
         * @param {Array} arg
         * @param {function} callback
         * @return void
         * */
        _defineProperty( global , 'for' , function ( arg , callback ) {
            if ( isarray( arg ) ) {
                foreach( arg , callback );
            }
            else if ( isobject( arg ) ) {
                forin( arg , callback );
            }
        } );
        
        /**
         * @readonly
         * @function smoothyIncrement
         * @param {Object} setting
         * @param {number} setting.begin
         * @param {number} setting.end
         * @param {function} setting.eachFrame
         * @param {function} [setting.onFinish]
         * @param {number} [setting.speed=500]
         * @return void
         * */
        _defineProperty( global , 'smoothyIncrement' , ( function () {
            
            var frameLatency = 1000 / 60;
            
            var requestFrame = global[ 'requestAnimationFrame' ] ||
                global[ 'webkitRequestAnimationFrame' ] ||
                global[ 'mozRequestAnimationFrame' ] ||
                global[ 'msRequestAnimationFrame' ];
            
            var cancelFrame = global[ 'cancelAnimationFrame' ] ||
                global[ 'webkitCancelAnimationFrame' ] ||
                global[ 'mozCancelAnimationFrame' ] ||
                global[ 'msCancelAnimationFrame' ];
            
            function ease ( n ) {
                return 0.5 * ( 1 - Math.cos( Math.PI * n ) );
            }
            
            function animate ( callframe ) {
                var frame , start;
                
                function loop () {
                    frame = requestFrame( loop );
                    callframe();
                }
                
                start = setImmediate( loop );
                
                return function () {
                    clearImmediate( start );
                    cancelFrame( frame );
                };
            }
            
            return function ( setting ) {
                
                setting = overrideObject( {
                    speed : 1000
                } , setting );
                
                var begin = setting.begin ,
                    end = setting.end ,
                    speed = setting.speed ,
                    callback = setting.eachFrame ,
                    callbackFinish = setting.onFinish;
                
                if ( !isinteger( speed ) || speed <= 0 ) {
                    speed = 1000;
                }
                
                speed = Math.round( speed / 1.8 );
                
                var tmp = begin;
                begin = Math.min( begin , end );
                end = Math.max( tmp , end );
                
                var newval , elapsed;
                
                var firstStep = true ,
                    lastStep = false ,
                    endAtNext = false;
                
                var beginValues = [] ,
                    beginTotal = 0 ,
                    total = 0;
                
                var elapsedMax = 1 ,
                    elapsedMin = 0.1;
                
                var totalScroll = end - begin ,
                    increment = Math.round( totalScroll / ( speed / frameLatency ) );
                
                if ( totalScroll <= increment ) {
                    return callback( end , end - begin );
                }
                
                var startTime = Date.now();
                
                var stop = animate( function () {
                    
                    /* stop process */
                    /* ---------------------- */
                    if ( endAtNext ) {
                        callback( end , end - begin );
                        
                        if ( isfunction( callbackFinish ) ) {
                            callbackFinish();
                        }
                        
                        return stop();
                    }
                    
                    /* calc process */
                    /* ---------------------- */
                    
                    elapsed = round( ( Date.now() - startTime ) / speed , 2 );
                    
                    if ( elapsed < elapsedMin ) {
                        elapsed = elapsedMin;
                    }
                    else if ( elapsed > elapsedMax ) {
                        elapsed = elapsedMax;
                    }
                    
                    if ( firstStep && elapsed == elapsedMax ) {
                        firstStep = false;
                    }
                    
                    /* begin process */
                    /* ---------------------- */
                    
                    newval = Math.round( increment * ease( elapsed ) );
                    
                    if ( newval < 1 ) {
                        newval = 1;
                    }
                    
                    /* middle process */
                    /* ---------------------- */
                    
                    if ( firstStep ) {
                        beginValues.push( newval );
                        beginTotal += newval;
                    }
                    
                    if ( !firstStep && !lastStep && ( totalScroll - total ) <= beginTotal ) {
                        lastStep = true;
                    }
                    
                    /* end process */
                    /* ---------------------- */
                    
                    if ( lastStep ) {
                        if ( beginValues.length ) {
                            newval = beginValues.pop();
                        }
                        else {
                            newval = 1;
                        }
                    }
                    
                    total += newval;
                    
                    /* request stop process */
                    /* ---------------------- */
                    
                    if ( lastStep && totalScroll - total <= newval ) {
                        endAtNext = true;
                    }
                    
                    /* callback */
                    /* ---------------------- */
                    
                    callback( begin += newval , newval );
                    
                } );
                
                return stop;
                
            };
            
        } )() );
        
        /**
         * @readonly
         * @function cacheHandler
         * @param {number} [n=500]
         * @return {!function}
         * */
        _defineProperty( global , 'cacheHandler' , function ( n ) {
            var c = [];
            !n && ( n = 500 );
            
            function SAVE ( k , v ) {
                if ( !/^_(rm|purge)$/g.test( k ) ) {
                    SAVE[ k ] = v;
                    c.push( k );
                    
                    if ( c.length > n ) {
                        delete SAVE[ c.shift() ];
                    }
                }
            }
            
            SAVE._rm = function ( k ) {
                var i;
                
                if ( ( i = c.indexOf( k ), i ) !== -1 ) {
                    delete SAVE[ k ];
                    c.splice( i , 1 );
                }
            };
            
            SAVE._purge = function () {
                var i = c.length;
                
                while ( i-- ) {
                    delete SAVE[ c.shift() ];
                }
            };
            
            return SAVE;
        } );
        
        /**
         * @readonly
         * @function requireJS
         * @param {string|Array} source
         * @param {function} callback
         * @return void
         * */
        _defineProperty( global , 'requireJS' , ( function () {
            var document = getdocument( global );
            
            function require ( src , callback ) {
                var script;
                
                function load ( e ) {
                    callback();
                    e && e.type == 'load' && script.removeEventListener( 'load' , load );
                }
                
                if ( document ) {
                    
                    if ( document.querySelector( 'script[src="' + src + '"]' ) ) {
                        return load();
                    }
                    
                    script = document.createElement( 'script' );
                    
                    script.setAttribute( 'src' , src );
                    script.setAttribute( 'async' , 'async' );
                    script.setAttribute( 'type' , 'text/javascript' );
                    
                    script.addEventListener( 'load' , load );
                    
                    document.head.appendChild( script );
                    
                }
            }
            
            return function ( source , callback ) {
                var scripts = toarray( source ) ,
                    caller = counterCallback( scripts.length , callback );
                
                foreach( scripts , function ( script ) {
                    require( script , caller );
                } );
            };
            
        } )() );
        
        /**
         * @readonly
         * @function requireCSS
         * @param {string|Array} source
         * @param {function} callback
         * @return void
         * */
        _defineProperty( global , 'requireCSS' , ( function () {
            var document = getdocument( global );
            
            function require ( href , callback ) {
                var link;
                
                function load ( e ) {
                    callback();
                    e && e.type == 'load' && link.removeEventListener( 'load' , load );
                }
                
                if ( document ) {
                    
                    if ( document.querySelector( 'link[href="' + href + '"]' ) ) {
                        return load();
                    }
                    
                    link = document.createElement( 'link' );
                    
                    link.setAttribute( 'href' , href );
                    link.setAttribute( 'rel' , 'stylesheet' );
                    
                    link.addEventListener( 'load' , load );
                    
                    document.head.appendChild( link );
                    
                }
            }
            
            return function ( source , callback ) {
                var links = toarray( source ) ,
                    caller = counterCallback( links.length , callback );
                
                foreach( links , function ( link ) {
                    require( link , caller );
                } );
            };
            
        } )() );
        
        /**
         * @readonly
         * @function htmlDOM
         * @param {string} str
         * @return {?(HTMLElement|Element|Node|array)}
         * */
        _defineProperty( global , 'htmlDOM' , ( function () {
            var document = getdocument( global );
            
            if ( !document ) {
                return function () {
                    return null;
                };
            }
            
            var parentOf = {
                "col" : "colgroup" ,
                "tr" : "tbody" ,
                "th" : "tr" ,
                "td" : "tr" ,
                "colgroup" : "table" ,
                "tbody" : "table" ,
                "thead" : "table" ,
                "tfoot" : "table" ,
                "dt" : "dl" ,
                "dd" : "dl" ,
                "figcaption" : "figure" ,
                "legend" : "fieldset" ,
                "fieldset" : "form" ,
                "keygen" : "form" ,
                "area" : "map" ,
                "menuitem" : "menu" ,
                "li" : "ul" ,
                "option" : "optgroup" ,
                "optgroup" : "select" ,
                "output" : "form" ,
                "rt" : "ruby" ,
                "rp" : "ruby" ,
                "summary" : "details" ,
                "track" : "video" ,
                "source" : "video" ,
                "param" : "object"
            };
            
            var _ = cacheHandler();
            
            function clone ( e ) {
                if ( isarray( e ) ) {
                    for ( var i = 0 , l = e.length , r = [] ; i < l ; i++ ) {
                        r.push( e[ i ].cloneNode( true ) );
                    }
                    return r;
                }
                
                if ( exist( e ) ) {
                    return e.cloneNode( true );
                }
                
                return null;
            }
            
            function child ( e ) {
                return e.children.length == 1 ? e.children[ 0 ] : Array.apply( null , e.children );
            }
            
            function createHTML ( str ) {
                var firstTag = ( /^(<[\s]*[\w]{1,15}[\s]*(\b|>)?)/gi.exec( str ) || [ '' ] )[ 0 ]
                    .replace( /\W/g , '' ).trim().toLowerCase() ,
                    parent , doc;
                
                if ( !firstTag ) {
                    return;
                }
                
                if ( !parentOf[ firstTag ] ) {
                    doc = document.createDocumentFragment();
                    parent = document.createElement( 'DIV' );
                    doc.appendChild( parent );
                    parent.innerHTML = str;
                    return child( parent );
                }
                
                /** @type {Node} */
                parent = createHTML( '<' + parentOf[ firstTag ] + '></' + parentOf[ firstTag ] + '>' );
                
                while ( parent.firstChild ) {
                    parent = parent.firstChild;
                }
                
                parent.innerHTML = str;
                
                return child( parent );
            }
            
            return function ( str ) {
                var r;
                
                str = str.trim();
                
                if ( _[ str ] ) {
                    return clone( _[ str ] );
                }
                
                if ( !/(<[\s]*(\/)?[\s]*[\w]{1,15}[\s]*(\/)?[\s]*(\b|>)?)/gi.test( str ) ) {
                    return null;
                }
                
                if ( /^(<\/[\s]*[\w]{1,15}[\s]*>)$/gi.test( str ) ) {
                    r = document.createElement( str.replace( /\W/g , '' ).toUpperCase() );
                }
                else {
                    r = createHTML( str );
                }
                
                return _( str , r ) , clone( r );
            };
        } )() );
        
    } )();
    
    /**
     * css-rule
     * @license MIT
     * @see {@link https://framagit.org/tla/css-rule}
     * */
    var cssConstructor = ( function () {
        
        function sniffer ( instance , callback ) {
            var random = '_' + Math.random().toString( 30 ).substring( 2 ) ,
                element = instance.document.createElement( 'div' );
            
            element.id = random;
            element.style.setProperty( 'width' , '10px' );
            instance.document.body.appendChild( element );
            
            function check () {
                if ( element.clientWidth == 20 ) {
                    callback();
                    instance.remove( '#' + random );
                    return instance.document.body.removeChild( element );
                }
                
                setTimeout( check , 10 );
            }
            
            instance.add( '#' + random , 'width: 20px !important;' );
            setTimeout( check , 10 );
        }
        
        function cssgen ( doc ) {
            !doc && ( doc = document );
            
            this.id = 0;
            this.index = {};
            this.timeout = [];
            this.document = doc;
            
            this.style = doc.createElement( 'STYLE' );
            this.style.setAttribute( 'type' , 'text/css' );
            this.style.appendChild( doc.createTextNode( "" ) );
            
            this.head = doc.getElementsByTagName( 'HEAD' )[ 0 ];
            this.head.appendChild( this.style );
            
            this.api = this.style.sheet;
            this.rules = this.api.cssRules;
            this.insertRule = 'insertRule' in this.api ? 'insertRule' : 'addRule';
            this.deleteRule = 'deleteRule' in this.api ? 'deleteRule' : 'removeRule';
        }
        
        cssgen.prototype.setTimeout = function ( fn , time ) {
            var id = setTimeout( function () {
                this.timeout.splice( this.timeout.indexOf( id ) , 1 );
                fn();
            } , time );
            this.timeout.push( id );
        };
        
        cssgen.prototype.clearTimeout = function () {
            this.timeout.forEach( function ( id ) {
                clearTimeout( id );
            } );
        };
        
        cssgen.prototype.removeByRule = function ( rule ) {
            var rules = Array.apply( null , this.rules ) , i;
            
            if ( ( i = rules.indexOf( rule ) , !!~i ) ) {
                this.api[ this.deleteRule ]( i );
                
                return true;
            }
            
            return false;
        };
        
        /**
         * @method cssgen.prototype.add
         * @param {string} selector
         * @param {string} rules
         * @param {function} [call]
         * @return {void}
         * */
        cssgen.prototype.add = function ( selector , rules , call ) {
            var rule;
            
            this.api[ this.insertRule ]( selector + '{' + rules + '}' , this.rules.length );
            
            rule = this.rules[ this.rules.length - 1 ];
            
            if ( !this.index[ selector ] ) {
                this.index[ selector ] = [];
            }
            
            this.index[ selector ].push( rule );
            
            if ( isfunction( call ) ) {
                sniffer( this , call );
            }
        };
        
        /**
         * @method cssgen.prototype.remove
         * @param {string} selector
         * @return {boolean}
         * */
        cssgen.prototype.remove = function ( selector ) {
            var result = true , array , i , l;
            
            if ( array = this.index[ selector ] ) {
                for ( i = 0, l = array.length ; i < l ; ) {
                    if ( !this.removeByRule( array[ i++ ] ) ) {
                        result = false;
                    }
                }
                
                delete this.index[ selector ];
            }
            
            return result;
        };
        
        /**
         * @method cssgen.prototype.toggle
         * @param {string} selector
         * @param {string} rules
         * @param {function} [call]
         * @return {void}
         * */
        cssgen.prototype.toggle = function ( selector , rules , call ) {
            this.remove( selector );
            this.add( selector , rules , call );
        };
        
        /**
         * @method cssgen.prototype.destroy
         * @return {void}
         * */
        cssgen.prototype.destroy = function () {
            this.clearTimeout();
            this.head.removeChild( this.style );
        };
        
        return cssgen;
        
    } )();
    
    
    var passiveEventHandler = function ( cap ) {
        return Boolean( cap );
    };
    
    if ( eventPassiveSupport ) {
        passiveEventHandler = function ( cap ) {
            return {
                capture : Boolean( cap ) ,
                passive : true
            };
        };
    }
    
    var _COUNTER = 0;
    var indexer = {};
    
    var isLeftClick = ( function () {
        
        function isEvent ( event ) {
            if ( exist( event ) ) {
                return ( event instanceof Event ) ||
                    ( exist( event.defaultEvent ) && event.defaultEvent instanceof Event ) ||
                    ( exist( event.originalEvent ) && event.originalEvent instanceof Event );
            }
            return false;
        }
        
        function isTouch ( event ) {
            return event.type.charAt( 0 ) === 't';
        }
        
        function isClick ( event ) {
            return event.type == 'click';
        }
        
        function isMouse ( event ) {
            return event.type.slice( 0 , 5 ) == 'mouse';
        }
        
        function isWhich ( event , n ) {
            return isset( event.which ) && event.which == n;
        }
        
        function isButton ( event , n ) {
            return isset( event.button ) && event.button == n;
        }
        
        return function ( event ) {
            return isEvent( event ) && ( isTouch( event ) && isWhich( event , 0 ) ) ||
                ( ( isClick( event ) || isMouse( event ) ) && ( isWhich( event , 1 ) || isButton( event , 0 ) ) );
        };
        
    } )();
    
    
    /******************************************
     *                   FN                   *
     ******************************************/
    function toRatio ( n ) {
        return n / 100;
    }
    
    function transform ( n ) {
        return 'transform:' + n + ';';
    }
    
    function isDisabled ( element ) {
        return !!closest( element , '[disabled]' );
    }
    
    function scale ( n ) {
        return transform( 'scale(' + n + ')' );
    }
    
    function transition ( prop , val ) {
        return 'transition-' + prop + ':' + val + ';';
    }
    
    function prevent ( e ) {
        e.preventDefault();
        e.stopImmediatePropagation();
    }
    
    // element size
    function getSize ( element ) {
        return {
            height : element.offsetHeight ,
            width : element.offsetWidth ,
            left : element.offsetLeft ,
            top : element.offsetTop
        };
    }
    
    function getScale ( element ) {
        var beMagicAndGFY = 0.015 ,
            tmp = window.getComputedStyle( element );
        
        tmp = ( tmp.getPropertyValue( 'transform' ) || '' ).replace( /[\s\u00A0]+/g , '' );
        
        if ( /^(matrix)/.test( tmp ) ) {
            return 'scale(' + ( +( tmp.slice( 7 , tmp.indexOf( ',' ) ) ) + beMagicAndGFY ) + ')';
        }
        
        return 'scale(' + ( +( tmp.slice( 6 , -1 ) ) + beMagicAndGFY ) + ')';
    }
    
    function getTouch ( event ) {
        if ( event = event[ 'touches' ] || event[ 'changedTouches' ] ) {
            return event;
        }
        return [];
    }
    
    // get mouse ( or finger ) position in the document
    function clientData ( event ) {
        var a;
        return event.type.charAt( 0 ) !== 't' ? {
            x : event.clientX ,
            y : event.clientY
        } : ( a = getTouch( event ), {
            x : a[ a.length - 1 ].clientX ,
            y : a[ a.length - 1 ].clientY
        } );
    }
    
    // get current max size of origin element and the mouse ( or finger ) position in the element
    function getData ( button , event ) {
        var size = button.getBoundingClientRect() ,
            client = clientData( event ) ,
            data = {
                width : size.width ,
                height : size.height ,
                y : client.y - size.top ,
                x : client.x - size.left ,
                max : Math.ceil( Math.sqrt( Math.pow( size.width , 2 ) + Math.pow( size.height , 2 ) ) ) * 2
            };
        
        data.calcX = data.x - ( data.max / 2 );
        data.calcY = data.y - ( data.max / 2 );
        
        return data;
    }
    
    function createBubble ( cls , document ) {
        var bubbleContainer = document.createElement( 'div' ) ,
            bubbleDown = document.createElement( 'div' ) ,
            bubbleUp = document.createElement( 'div' );
        
        bubbleDown.classList.add( 'bubble-' + cls );
        bubbleUp.classList.add( 'bubble-' + cls );
        
        bubbleDown.setAttribute( 'identifier' , 'bubble-' + cls );
        
        bubbleContainer.appendChild( bubbleDown );
        bubbleContainer.appendChild( bubbleUp );
        
        return bubbleContainer;
    }
    
    // move and resize bubble container
    function moveContainer () {
        var comput = window.getComputedStyle( this.element ) ,
            prop = JSON.stringify( comput ) ,
            size;
        
        if ( ( this.lastCSS && this.lastCSS == prop ) && !this.forceMove ) {
            return;
        }
        
        this.lastCSS = prop;
        this.css.remove( '.' + this.className + ' > .awesome-bubble-container' );
        
        if ( { fixed : 1 , relative : 1 , absolute : 1 }[ comput.getPropertyValue( 'position' ).toLowerCase().trim() ] ) {
            return this.css.add( '.' + this.className + ' > .awesome-bubble-container' , 'top:0; left:0; width:100%; height:100%' );
        }
        
        size = getSize( this.element );
        
        this.css.add( '.' + this.className + ' > .awesome-bubble-container' , '' +
            'top:' + size.top + 'px;' +
            'left:' + size.left + 'px;' +
            'width:' + size.width + 'px;' +
            'height:' + size.height + 'px' );
    }
    
    function buildGenericCss ( document ) {
        if ( document.awesomeBubbleCssBuild ) {
            return;
        }
        
        var css = new cssConstructor( document );
        
        // *
        css.add( '.awesome-bubble-pattern > div.awesome-bubble-container ,' +
            '.awesome-bubble-pattern > div.awesome-bubble-container div' ,
            'border:none !important;' +
            'margin:0 !important;' +
            'padding:0 !important;' +
            'overflow:hidden !important;' +
            'pointer-events:none !important'
        );
        
        // bubble container
        css.add( '.awesome-bubble-pattern > div.awesome-bubble-container' , 'position:absolute;border-radius:inherit' );
        
        // bubble sub container
        css.add( '.awesome-bubble-pattern > div.awesome-bubble-container > div' ,
            transition( 'property' , 'opacity' ) +
            transition( 'timing-function' , 'ease-out' ) +
            'position:absolute;top:0;left:0;width:100%;height:100%' );
        css.add( '.awesome-bubble-pattern > div.awesome-bubble-container > div.awesome-bubble-remove' , 'opacity:0' );
        
        // bubble *
        css.add( '.awesome-bubble-pattern > div.awesome-bubble-container > div > div' ,
            transition( 'property' , 'transform , background-color' ) +
            transition( 'timing-function' , 'ease-out' ) +
            'border-radius:100%;position:absolute' );
        
        // bubble :first
        css.add( '.awesome-bubble-pattern > div.awesome-bubble-container > div > div:first-child' , scale( 0 ) );
        
        // bubble :last
        css.add( '.awesome-bubble-pattern > div.awesome-bubble-container > div > div:last-child' , 'display:none' );
        
        // bubble water
        css.add( '.awesome-bubble-pattern > div.awesome-bubble-container > div.awesome-bubble-water-remove' , 'opacity:0' );
        
        document.awesomeBubbleCssBuild = true, css = null;
    }
    
    /******************************************
     *              HYBRID EVENT              *
     ******************************************/
    var listen = [] ,
        handler = [] ,
        hackMouse = ( function () {
            var api = {};
            
            [ 'mousedown' , 'mousemove' , 'mouseup' ].forEach( function ( type ) {
                var cur = api[ type ] = {
                    auth : true ,
                    idTime : null ,
                    block : function ( safe ) {
                        cur.auth = false;
                        clearTimeout( cur.idTime );
                        cur.idTime = setTimeout( function () {
                            cur.auth = true;
                        } , safe ? 200 : 800 );
                    }
                };
            } );
            
            return {
                touch : function ( call , event ) {
                    switch ( event.type ) {
                        
                        case 'touchstart':
                            api.mousedown.block();
                            api.mousemove.block();
                            api.mouseup.block();
                            event.target.addEventListener( 'contextmenu' , prevent );
                            break;
                        
                        case 'touchmove':
                            api.mousemove.block();
                            break;
                        
                        case 'touchend':
                        case 'touchleave':
                        case 'touchcancel':
                            api.mouseup.block();
                            event.target.removeEventListener( 'contextmenu' , prevent );
                            break;
                        
                    }
                    
                    call.call( this , event );
                } ,
                
                mouse : function ( call , event ) {
                    if ( event.type !== 'mousemove' && !isLeftClick( event ) ) {
                        return;
                    }
                    switch ( event.type ) {
                        
                        case 'mouseup':
                            api.mousemove.block( true );
                            break;
                        
                    }
                    
                    if ( api[ event.type ].auth ) {
                        call.call( this , event );
                    }
                }
            };
        } )();
    
    function type ( type ) {
        var eTouch , eMouse;
        switch ( type ) {
            
            case 'down':
                eTouch = 'touchstart';
                eMouse = 'mousedown';
                break;
            
            case 'move':
                eTouch = 'touchmove';
                eMouse = 'mousemove';
                break;
            
            case 'up':
                eTouch = 'touchend,touchleave,touchcancel';
                eMouse = 'mouseup';
                break;
            
        }
        
        return { touch : eTouch , mouse : eMouse };
    }
    
    function on ( e , d , f , c ) {
        var mouse = hackMouse.mouse.bind( null , f ) ,
            touch = hackMouse.touch.bind( null , f );
        
        e = type( e );
        
        handler.push( f );
        listen.push( { touch : touch , mouse : mouse } );
        
        e.touch.split( ',' ).forEach( function ( e ) {
            d.addEventListener( e , touch , passiveEventHandler( c ) );
        } );
        d.addEventListener( e.mouse , mouse , passiveEventHandler( c ) );
    }
    
    function off ( e , d , f , c ) {
        var i = handler.indexOf( f ) , l;
        
        if ( i !== -1 ) {
            l = listen[ i ], e = type( e );
            
            handler.splice( i , 1 );
            listen.splice( i , 1 );
            
            e.touch.split( ',' ).forEach( function ( e ) {
                d.removeEventListener( e , l.touch , c );
            } );
            d.removeEventListener( e.mouse , l.mouse , c );
        }
    }
    
    function onDown ( d , f , c ) {
        on( 'down' , d , f , c );
    }
    
    function offDown ( d , f , c ) {
        off( 'down' , d , f , c );
    }
    
    function onMove ( d , f , c ) {
        on( 'move' , d , f , c );
    }
    
    function offMove ( d , f , c ) {
        off( 'move' , d , f , c );
    }
    
    function onUp ( d , f , c ) {
        on( 'up' , d , f , c );
    }
    
    function offUp ( d , f , c ) {
        off( 'up' , d , f , c );
    }
    
    /******************************************
     *          BUBBLE CONSTRUCTOR            *
     ******************************************/
    function _CONSTRUCT ( current , element , document ) {
        this.bubble = [];
        this.active = true;
        this.uniqBuble = 0;
        this.timeout = null;
        this.bubbleNumber = 0;
        this.bubbleNumberEnd = 0;
        this.forceMove = false;
        this.cleartimeout = null;
        this.forExpandBubble = [];
        this.element = element, element = null;
        this.css = new cssConstructor( document );
        this.container = document.createElement( 'div' );
        this.focuced = 'awesome-bubble-focuced-' + current;
        this.className = 'awesome-bubble-' + current, current = null;
        
        this.current = current;
        indexer[ current ] = this;
        
        // setting
        this.setting = {
            number : 1 ,
            opacity : 0.3 ,
            duration : 700 ,
            interval : 100 ,
            bubbleSize : 1 ,
            bubbleEffect : true ,
            
            waterEffect : false ,
            intervalWater : 100 ,
            waterDuration : 1000 ,
            bubbleWaterSize : 0.1 ,
            waterOnlyWhenPress : false ,
            
            material : false ,
            stayOnFocus : false ,
            
            colorEnd : [] ,
            color : [ 'cornflowerblue' ]
        };
        
        this.onresize = function () { this.forceMove = true; }.bind( this );
        window.addEventListener( 'resize' , this.onresize , passiveEventHandler( true ) );
        
        // on put down ( mousedown , touchstart )
        this.putDown = function ( e ) {
            var i , data ,
                reflow , loop ,
                fake = e.type == 'fake' ,
                bubble = this.setting.bubbleEffect ,
                stayonfocus = this.setting.stayOnFocus && bubble ,
                material = this.setting.material && !this.setting.stayOnFocus;
            
            if ( isDisabled( this.element ) || ( stayonfocus && this.element.classList.contains( this.focuced ) ) ) {
                return;
            }
            
            !e && ( e = window.event );
            data = getData( this.element , e );
            
            if ( data.x >= 0 && data.y >= 0 && data.x <= data.width && data.y <= data.height ) {
                
                clearTimeout( this.timeout );
                clearTimeout( this.cleartimeout );
                
                if ( !stayonfocus && !fake ) {
                    onUp( document , this.putUp , true );
                }
                
                if ( stayonfocus ) {
                    this.element.classList.add( this.focuced );
                    onDown( document , this.putOut , true );
                }
                else if ( this.setting.waterEffect && this.setting.waterOnlyWhenPress ) {
                    onMove( this.element , this.putMove , true );
                }
                
                if ( bubble ) {
                    i = 0;
                    
                    reflow = function ( d ) {
                        var bubble = this.reflow( data , d );
                        bubble.firstElementChild.classList.add( material ? 'material' : 'basic' );
                        loop();
                    }.bind( this );
                    
                    loop = function () {
                        if ( this.setting.number > ++i ) {
                            reflow( this.setting.interval * i );
                        }
                        else {
                            if ( !this.setting.stayOnFocus && !this.setting.waterEffect ) {
                                if ( fake ) {
                                    return this.cleartimeout = setTimeout( this.putUp , 100 );
                                }
                                
                                this.cleartimeout = setTimeout( this.putUp , this.setting.duration * ( material ? 10.3 : 2 ) );
                            }
                        }
                    }.bind( this );
                    
                    // ensure the bubble container is the last child
                    this.container.nextSibling && this.element.appendChild( this.container );
                    
                    moveContainer.call( this );
                    
                    reflow( 0 );
                }
                
            }
        }.bind( this );
        
        // water effect
        this.water = [];
        this.putMoveBlocker = false;
        this.putMoveBlockerTimeout = null;
        this.putMove = function ( e ) {
            var water = {};
            
            if ( !isDisabled( this.element ) ) {
                
                if ( !this.putMoveBlocker ) {
                    this.putMoveBlocker = true;
                    moveContainer.call( this );
                    !e && ( e = window.event );
                    
                    this.reflow( getData( this.element , e ) , false , true , function ( bubble ) {
                        var child = bubble.firstElementChild ,
                            id = child.getAttribute( 'identifier' );
                        
                        water.bubble = bubble;
                        
                        child.classList.add( 'awesome-bubble-water-expand' );
                        //bubble.classList.add( 'awesome-bubble-water-remove' );
                        
                        water.timeout = setTimeout( function () {
                            
                            this.container.removeChild( bubble );
                            
                            this.css.remove( '.' + this.className + ' > .awesome-bubble-container > div > div.' + id );
                            this.css.remove( '.' + this.className + ' > .awesome-bubble-container > div > div.' + id + '.awesome-bubble-color-end' );
                            
                            this.water.splice( this.water.indexOf( water ) , 1 ), bubble = water = null;
                            
                        }.bind( this ) , this.setting.waterDuration * 2 );
                    }.bind( this ) );
                    
                    this.water.push( water );
                    
                    this.putMoveBlockerTimeout = setTimeout( function () {
                        this.putMoveBlocker = false;
                    }.bind( this ) , this.setting.intervalWater );
                }
                
            }
        }.bind( this );
        
        // on put up ( mouseup , touchend )
        this.putUp = function () {
            var material = this.setting.material && !this.setting.stayOnFocus;
            
            clearTimeout( this.timeout );
            clearTimeout( this.cleartimeout );
            
            if ( this.setting.waterEffect && this.setting.waterOnlyWhenPress ) {
                this.putMoveBlocker = false;
                clearTimeout( this.putMoveBlockerTimeout );
                offMove( this.element , this.putMove , true );
            }
            
            offUp( document , this.putUp , true );
            
            // acceleration de la bulle ( explosion )
            if ( this.bubble.length ) {
                this.forExpandBubble.forEach( function ( bubbleContainer , index ) {
                    
                    var bubbleDown = bubbleContainer.firstElementChild ,
                        bubbleUp = bubbleContainer.lastElementChild ,
                        cls = bubbleDown.getAttribute( 'identifier' );
                    
                    if ( !material ) {
                        bubbleContainer.classList.add( 'awesome-bubble-remove' );
                        bubbleUp.classList.add( 'awesome-bubble-expand' );
                        return;
                    }
                    
                    this.css.add( '.awesome-bubble-pattern > div.awesome-bubble-container > div > div:last-child.' + cls ,
                        transform( getScale( bubbleDown ) ) ,
                        function () {
                            
                            bubbleDown.style.display = 'none';
                            bubbleUp.style.display = 'block';
                            
                            this.css.add( '.awesome-bubble-pattern > div.awesome-bubble-container > div > div:last-child.' + cls + '.awesome-bubble-expand' ,
                                scale( this.setting.bubbleSize ) +
                                transition( 'duration' , this.setting.duration + 'ms' ) ,
                                function () {
                                    
                                    bubbleContainer.classList.add( 'awesome-bubble-remove' );
                                    bubbleUp.classList.add( 'awesome-bubble-expand' );
                                    
                                } );
                            
                        }.bind( this ) );
                    
                    this.forExpandBubble.splice( index , 1 );
                    
                }.bind( this ) );
            }
            
            // when the bubble is fade, clear all
            this.timeout = setTimeout( this.clear.bind( this ) , this.setting.duration * 2 );
        }.bind( this );
        
        this.putOut = function ( e , f ) {
            if ( ( f && this.element.classList.contains( this.focuced ) ) || ( e && !closest( e.target , '.' + this.focuced ) ) ) {
                offDown( document , this.putOut , true );
                this.element.classList.remove( this.focuced );
                this.putUp();
            }
        }.bind( this );
    }
    
    // clear class and custom css
    _CONSTRUCT.prototype.clear = function ( hard ) {
        var length , remove , id;
        if ( ( length = this.bubble.length ) ) {
            while ( ( remove = this.bubble[ --length ] ) ) {
                this.container.removeChild( remove );
                if ( !hard ) {
                    id = remove.firstElementChild.getAttribute( 'identifier' );
                    this.css.remove( '.' + this.className + ' > .awesome-bubble-container > div > div.' + id );
                    this.css.remove( '.awesome-bubble-pattern > div.awesome-bubble-container > div > div:last-child.' + id );
                    this.css.remove( '.' + this.className + ' > .awesome-bubble-container > div > div.' + id + '.awesome-bubble-color-end' );
                    this.css.remove( '.awesome-bubble-pattern > div.awesome-bubble-container > div > div:last-child.' + id + '.awesome-bubble-expand' );
                }
            }
            this.uniqBuble = this.bubbleNumber = 0;
            this.bubble = [];
        }
    };
    
    // resize bubble and bubble container, and start it
    _CONSTRUCT.prototype.reflow = function ( data , delay , water , call ) {
        var self = this ,
            colorEnd = this.setting.colorEnd.length ,
            bubble = createBubble( ++this.uniqBuble , document );
        
        function ready () {
            var bubbleDown = bubble.firstElementChild;
            
            if ( self.setting.colorEnd.length ) {
                bubbleDown.classList.add( 'awesome-bubble-color-end' );
            }
            
            if ( water ) {
                return call( bubble );
            }
            
            bubbleDown.classList.add( 'awesome-bubble-expand' );
        }
        
        // bubble position ( the mouse cursor ) and size
        this.css.add( '.' + this.className + ' > .awesome-bubble-container > div > div.bubble-' + this.uniqBuble ,
            'top:' + data.calcY + 'px;' +
            'left:' + data.calcX + 'px;' +
            'width:' + data.max + 'px;' +
            'height:' + data.max + 'px;' +
            ( delay ? transition( 'delay' , delay + 'ms' ) : '' ) +
            'background-color:' + ( this.bubbleNumber >= this.setting.color.length &&
            ( this.bubbleNumber = 0 ), this.setting.color[ this.bubbleNumber++ ] ) ,
            !colorEnd ? ready : null );
        
        if ( colorEnd ) {
            this.css.add(
                // bubble
                '.' + this.className + ' > .awesome-bubble-container > div > ' +
                'div.bubble-' + this.uniqBuble + '.awesome-bubble-color-end' ,
                
                // background
                'background-color:' + ( this.bubbleNumberEnd >= this.setting.colorEnd.length &&
                ( this.bubbleNumberEnd = 0 ), this.setting.colorEnd[ this.bubbleNumberEnd++ ] ) ,
                
                // callback
                ready
            );
        }
        
        if ( !water ) {
            this.bubble.push( bubble );
            this.forExpandBubble.push( bubble );
        }
        
        this.container.appendChild( bubble );
        
        return bubble;
    };
    
    _CONSTRUCT.prototype.clearwater = function () {
        this.water.forEach( function ( water ) {
            var id = water.bubble.firstElementChild.getAttribute( 'identifier' );
            clearTimeout( water.timeout );
            this.container.removeChild( water.bubble );
            this.css.remove( '.' + this.className + ' > .awesome-bubble-container > div > div.' + id );
            this.css.remove( '.' + this.className + ' > .awesome-bubble-container > div > div.' + id + '.awesome-bubble-color-end' );
        }.bind( this ) );
        this.water = [];
        clearTimeout( this.putMoveBlockerTimeout );
        offMove( this.element , this.putMove , true );
    };
    
    _CONSTRUCT.prototype.disable = function () {
        if ( this.active ) {
            this.active = false;
            offDown( this.element , this.putDown , true );
            this.setting.waterEffect && offMove( this.element , this.putMove , true );
        }
    };
    
    _CONSTRUCT.prototype.enable = function () {
        if ( !this.active ) {
            this.active = true;
            onDown( this.element , this.putDown , true );
            ( this.setting.waterEffect && !this.setting.waterOnlyWhenPress ) && onMove( this.element , this.putMove , true );
        }
    };
    
    _CONSTRUCT.prototype.emulate = function () {
        var d;
        if ( this.active ) {
            d = this.element.getBoundingClientRect();
            
            return this.putDown( {
                type : 'fake' ,
                clientX : Math.round( d.left + ( d.width / 2 ) ) ,
                clientY : Math.round( d.top + ( d.height / 2 ) )
            } );
        }
    };
    
    // kill _awesomeBubble instance
    _CONSTRUCT.prototype.destroy = function () {
        delete indexer[ this.current ];
        
        // clearTimeout
        clearTimeout( this.timeout );
        clearTimeout( this.cleartimeout );
        
        // remove element
        this.clearwater();
        this.clear( true );
        this.element.classList.remove( this.className );
        this.element.classList.remove( 'awesome-bubble-pattern' );
        this.container.parentNode && this.container.parentNode.removeChild( this.container );
        
        // remove css rules
        if ( this.css ) {
            this.css.destroy();
            this.css = null;
        }
        
        // remove event
        offUp( document , this.putUp , true );
        offMove( this.element , this.putMove , true );
        offDown( this.element , this.putDown , true );
        window.removeEventListener( 'resize' , this.onresize , true );
    };
    
    var validate = ( function () {
        var minint = 30;
        
        function bool ( defaultValue , newValue ) {
            if ( isset( newValue ) ) {
                return !!newValue;
            }
            
            return defaultValue;
        }
        
        function ratio ( defaultValue , newValue ) {
            if ( isset( newValue ) ) {
                newValue = parseFloat( newValue );
                
                if ( !isNaN( newValue ) ) {
                    return toRatio( newValue );
                }
            }
            
            return defaultValue;
        }
        
        function int ( defaultValue , newValue ) {
            if ( isset( newValue ) ) {
                newValue = parseInt( newValue );
                
                if ( !isNaN( newValue ) ) {
                    return newValue < 1 ? 1 : newValue;
                }
            }
            
            return defaultValue;
        }
        
        function minInt ( defaultValue , newValue ) {
            if ( isset( newValue ) ) {
                newValue = parseInt( newValue );
                
                if ( !isNaN( newValue ) ) {
                    return newValue < minint ? minint : newValue;
                }
            }
            
            return defaultValue;
        }
        
        function validString ( val ) {
            return typeof val == 'string' && !!val.trim();
        }
        
        function array ( defaultValue , newValue ) {
            if ( Array.isArray( newValue ) ) {
                return newValue.filter( validString );
            }
            
            return validString( newValue ) ? [ newValue ] : defaultValue;
        }
        
        return {
            
            material : bool ,
            stayOnFocus : bool ,
            waterEffect : bool ,
            bubbleEffect : bool ,
            waterOnlyWhenPress : bool ,
            
            opacity : ratio ,
            bubbleSize : ratio ,
            bubbleWaterSize : ratio ,
            
            duration : minInt ,
            interval : minInt ,
            intervalWater : minInt ,
            waterDuration : minInt ,
            
            color : array ,
            colorEnd : array ,
            
            number : int
            
        };
        
    } )();
    
    _CONSTRUCT.prototype.setParams = function ( name , value , init ) {
        this.setting[ name ] = validate[ name ]( this.setting[ name ] , value );
        
        if ( !init && this[ 'setStylesheet_' + name ] ) {
            this[ 'setStylesheet_' + name ]();
        }
    };
    
    _CONSTRUCT.prototype.setStylesheet_duration = function () {
        var tmp;
        
        tmp = '.' + this.className + ' > .awesome-bubble-container > div > div.material:first-child';
        this.css.toggle( tmp , transition( 'duration' , ( this.setting.duration * 10 ) + 'ms' ) );
        
        tmp = '.' + this.className + ' > .awesome-bubble-container > div > div.basic:first-child';
        this.css.toggle( tmp , transition( 'duration' , this.setting.duration + 'ms' ) );
        
        tmp = '.' + this.className + ' > .awesome-bubble-container > div.awesome-bubble-remove';
        this.css.toggle( tmp , transition( 'duration' , this.setting.duration + 'ms' ) );
    };
    
    _CONSTRUCT.prototype.setStylesheet_waterDuration = function () {
        var tmp;
        
        tmp = '.' + this.className + ' > .awesome-bubble-container > div > div.awesome-bubble-water-expand';
        this.css.toggle( tmp , transition( 'duration' , this.setting.waterDuration + 'ms' ) );
        
        tmp = '.' + this.className + ' > .awesome-bubble-container > div.awesome-bubble-water-remove';
        this.css.toggle( tmp , transition( 'duration' , this.setting.waterDuration + 'ms' ) );
    };
    
    _CONSTRUCT.prototype.setStylesheet_opacity = function () {
        var tmp;
        
        tmp = '.' + this.className + ' > div.awesome-bubble-container > div > div';
        this.css.toggle( tmp , 'opacity:' + this.setting.opacity );
    };
    
    _CONSTRUCT.prototype.setStylesheet_bubbleSize = function () {
        var tmp;
        
        tmp = '.' + this.className + ' > div.awesome-bubble-container > div > div:first-child.awesome-bubble-expand';
        this.css.toggle( tmp , scale( this.setting.bubbleSize ) );
    };
    
    _CONSTRUCT.prototype.setStylesheet_bubbleWaterSize = function () {
        var tmp;
        
        tmp = '.' + this.className + ' > div.awesome-bubble-container > div > div:first-child.awesome-bubble-water-expand';
        this.css.toggle( tmp , scale( this.setting.bubbleWaterSize ) );
    };
    
    
    /******************************************
     *                 CALLER                 *
     ******************************************/
    window._awesomeBubble = function ( element , setting ) {
        var doc = getdocument( element ) , self;
        
        if ( element.classList.contains( 'awesome-bubble-pattern' ) ) {
            return;
        }
        
        if ( setting && setting.document ) {
            doc = getdocument( setting.document ) || doc;
        }
        
        self = new _CONSTRUCT( ++_COUNTER , element , doc );
        
        buildGenericCss( doc );
        
        // add class
        self.container.classList.add( 'awesome-bubble-container' );
        self.element.classList.add( 'awesome-bubble-pattern' );
        self.element.classList.add( self.className );
        
        // get option
        if ( setting ) {
            // validate settings
            for ( var i in validate ) {
                self.setParams( i , setting[ i ] , true );
            }
        }
        
        [
            'opacity' ,
            'duration' ,
            'bubbleSize' ,
            'waterDuration' ,
            'bubbleWaterSize'
        ].forEach( function ( name ) {
            self[ 'setStylesheet_' + name ]();
        } );
        
        // append element
        self.element.appendChild( self.container );
        
        // listen event
        onDown( self.element , self.putDown , true );
        
        if ( self.setting.waterEffect && !self.setting.waterOnlyWhenPress && !self.setting.stayOnFocus ) {
            onMove( self.element , self.putMove , true );
        }
        
        return element = setting = null, self;
    };
    
} )();

